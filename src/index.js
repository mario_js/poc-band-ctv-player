import 'core-js';
import React from 'react';
import xdk from '@accedo/xdk-core';
import { render } from 'react-dom';

import App from './App';
import initXDK from './config/initXdk';

import './config/preflight';

initXDK()
  .catch((error) => {
    console.error(error)
  })
  .finally(() => {
    const platform = xdk.system.getDeviceType();
    console.log(`XDK device onload, platform: ${platform}`);

    render(<App platform={platform} />, document.getElementById('root'));
  })