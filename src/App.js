import React, { useCallback, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { vKey } from '@accedo/xdk-virtual-key';
import { environment } from '@accedo/xdk-core';

import vw from './utils/vw';
import styles from './theme/app.module.scss';

const {
  OK,
  PLAY,
  PAUSE,
  PLAY_PAUSE,
  FF,
  RW
} = vKey;
const ON_LIVE_URL = 'https://www.dailymotion.com/embed/video/x5eva8m';
const VOD_URL = 'https://www.dailymotion.com/embed/video/x84qof0?autoplay=1';
const PODCAST_URL = 'https://widget.spreaker.com/player?show_id=4902489';

const media = {
  onLive: {
    name: 'On live',
    src: ON_LIVE_URL,
    id: 'x5eva8m'
  },
  vod: {
    name: 'VOD',
    src: VOD_URL,
    id: 'x84qof0'
  },
  podcast: {
    name: 'Podcast',
    src: PODCAST_URL
  },
}

const { dailymotion } = window;
const VIDEO_ID = 'poc_dailymotion_player';

const App = ({ platform }) => {
  const playerRef = useRef();
  const getPlayerState = async () => playerRef.current?.getState();

  const onPlay = () => {
    playerRef.current?.play();
  }

  const onPause = () => {
    playerRef.current?.pause();
  }

  const onTogglePlay = useCallback(async () => {
    const playerState = await getPlayerState();
    if (!playerState) {
      return;
    }

    // playerRef.current?.togglePlay()
    if (playerState.playerIsPaused) {
      onPlay()
    } else {
      onPause();
    }
  }, []);

  const onSeek = useCallback(async (delta) => {
    if (!playerRef.current) {
      return;
    }

    const { videoTime = 0} = await getPlayerState();
    // const { currentTime = 0 } = playerRef.current;
    playerRef.current.seek(videoTime + delta);
  }, [])

  const onRW = useCallback(() => {
    onSeek(-15);
  }, [onSeek]);

  const onFF = useCallback(() => {
    onSeek(15);
  }, [onSeek]);

  const videoHandler = useCallback(({ id }) => {
    console.log(id);
    switch (id) {
      case PLAY_PAUSE.id:
      case OK.id:
        onTogglePlay();
        break;
      case PLAY.id:
        onPlay();
        break;
      case PAUSE.id:
        onPause();
        break;
      case RW.id:
        onRW();
        break;
      case FF.id:
        onFF();
        break;
      default:
        break;
    }
  }, [onTogglePlay, onRW, onFF]);

  const onVideoStart = (videoState) => {
    setTimeout(() => {
      console.log('Unmutting site');
      playerRef.current.setMute(false);
    }, 1000)
    // if (videoState?.playerAutoplayStatus === 'Resolved' && videoState?.playerMuted) {
    // }
  }

  useEffect(() => {
    dailymotion.createPlayer(VIDEO_ID, {
      video: media.vod.id
    })
    .then((player) => {
      playerRef.current = player
      playerRef.current.on(dailymotion.events.VIDEO_START, onVideoStart, { once: true });
      global.window.player = playerRef
    })

  }, []);

  // useEffect(() => {
  //   playerRef.current = DM.player(document.getElementById(VIDEO_ID), {
  //     video: media.onLive.id,
  //     params: {
  //       mute: false
  //     }
  //   })
  // }, []);

  useEffect(() => {
    environment.addEventListener(environment.SYSTEM.KEYDOWN, videoHandler);
    return () => {
      environment.removeEventListener(environment.SYSTEM.KEYDOWN, videoHandler); 
    }
  }, [videoHandler])

  return (
    <>
      <div className={styles.container}>
        <div className={styles.message}>
          { platform && <p>App running on {platform}</p> }
          <p>Current: {media.vod.name} ({media.vod.src})</p>
          <p>Click on 'OK' button to start the player.</p>
        </div>
      </div>

      <div
        id={VIDEO_ID}
        style={{
          position: 'absolute',
          bottom: vw(405)
        }}
      />
    </>
  );
}

App.propTypes = {
  platform: PropTypes.string
}

export default App;
