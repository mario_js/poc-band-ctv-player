import 'core-js';
import 'regenerator-runtime/runtime';

import xdkConfigStore from '@accedo/xdk-config';
import CONFIG from './xdk.config';

xdkConfigStore.load(CONFIG);