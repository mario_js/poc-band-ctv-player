import { recode } from '@accedo/xdk-virtual-key';
const cookieName = '@accedo/vdkweb-viki';

const COOKIE_ONLY = {
  cookieName
};

const isBravia = system => {
  const model = `${system.getModel().toLowerCase()}`;

  console.debug(`model is ${model}`);

  return model.includes('bravia');
};

const COOKIE_ONLY_ANDROID = {
  ...COOKIE_ONLY,
  tvkey: [
    {
      condition: isBravia,
      map: recode('MediaPlayPause', 'MediaPause')
    }
  ]
};

const CONFIG = {
  devices: {
    packages: [
      {
        id: 'workstation',
        detection: () =>
          import('@accedo/xdk-device-workstation/esm/detection.js'),
        defaultConfig: () =>
          import('@accedo/xdk-device-workstation/esm/defaultConfig.js'),
        DevicePackage: () =>
          import('@accedo/xdk-device-workstation/esm/DevicePackage.js')
      },
      {
        id: 'lg-webos',
        detection: () =>
          import('@accedo/xdk-device-lg-webos/esm/detection.js'),
        defaultConfig: () =>
          import('@accedo/xdk-device-lg-webos/esm/defaultConfig.js'),
        DevicePackage: () =>
          import('@accedo/xdk-device-lg-webos/esm/DevicePackage.js')
      },
      {
        id: 'samsung-tizen',
        detection: () =>
          import('@accedo/xdk-device-samsung-tizen/esm/detection.js'),
        defaultConfig: () =>
          import('@accedo/xdk-device-samsung-tizen/esm/defaultConfig.js'),
        DevicePackage: () =>
          import('@accedo/xdk-device-samsung-tizen/esm/DevicePackage.js')
      },
      {
        id: 'android-webview',
        detection: () =>
          import('@accedo/xdk-device-android-webview/esm/detection.js'),
        defaultConfig: () =>
          import('@accedo/xdk-device-android-webview/esm/defaultConfig.js'),
        DevicePackage: () =>
          import('@accedo/xdk-device-android-webview/esm/DevicePackage.js')
      }
    ],
    details: {
      'android-webview': COOKIE_ONLY_ANDROID
    }
  },
  logging: {
    level: -1
  }
};

export default CONFIG;