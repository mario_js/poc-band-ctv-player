import xdk from '@accedo/xdk-core';
import './preflight';

const initXDK = async () => {
  await xdk.load();
}

export default initXDK;